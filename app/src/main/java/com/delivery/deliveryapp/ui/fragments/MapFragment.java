package com.delivery.deliveryapp.ui.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delivery.deliveryapp.ui.activities.MainCommunicator;
import com.delivery.deliveryapp.viewmodels.MainViewModel;
import com.delivery.deliveryapp.R;
import com.delivery.deliveryapp.models.State;
import com.delivery.deliveryapp.models.Stop;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;


public class MapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap googleMap;
    private MainViewModel model;
    private int placeOvalSize;
    private int placeTextSize;
    private Paint paint;
    private MainCommunicator communicator;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof MainCommunicator){
            communicator = ((MainCommunicator) context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        communicator = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        placeOvalSize = getResources().getDimensionPixelSize(R.dimen.place_oval_size);
        placeTextSize = getResources().getDimensionPixelSize(R.dimen.place_num_size);
        paint = new Paint();
        paint.setStrokeWidth(placeOvalSize / 12f);
        paint.setTextAlign(Paint.Align.CENTER);
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        LatLng defaultLatLng = new LatLng(40.177626, 44.512458);
        CameraUpdate center = CameraUpdateFactory.newLatLng(defaultLatLng);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);
        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);
        googleMap.setMinZoomPreference(12);

        googleMap.setOnInfoWindowClickListener(marker -> {
            model.updateStopsStates(((Stop) marker.getTag()));
            communicator.scrollToStops();
        });

        model.getStopsData().observe(this, stops->{
            createStopPlaces(stops);
        });
    }

    private void createStopPlaces(List<Stop> list){
        googleMap.clear();
        for (Stop stop: list){
            Bitmap bitmap = Bitmap.createBitmap((int) (placeOvalSize / 1.32f), placeOvalSize, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            paint.setColor(getContext().getColor(stop.getState() == State.FINISHED ? R.color.blue : stop.getState() == State.IN_PROGRESS ? R.color.pink : R.color.gray_2));
            canvas.drawOval(0, 0, placeOvalSize / 1.32f, placeOvalSize / 1.32f, paint);
            canvas.drawLine(placeOvalSize / 2.64f, placeOvalSize / 1.32f, placeOvalSize / 2.64f, placeOvalSize, paint);
            paint.setColor(getContext().getColor(R.color.white));
            paint.setTextSize(placeTextSize);
            int yPos = (int) ((placeOvalSize / 1.32f / 2) - ((paint.descent() + paint.ascent()) / 2)) ;
            canvas.drawText(String.valueOf(list.indexOf(stop) + 1), placeOvalSize / 2.64f, yPos, paint);
            Marker place = googleMap.addMarker(new MarkerOptions().position(new LatLng(stop.getLatitude(), stop.getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(bitmap)).title(stop.getAddress() + ", " + stop.getCity()));
            place.setTag(stop);
        }
    }
}
