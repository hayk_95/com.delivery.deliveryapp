package com.delivery.deliveryapp.ui.activities;

import android.os.Bundle;

import com.delivery.deliveryapp.R;
import com.delivery.deliveryapp.viewmodels.MainViewModel;
import com.google.android.material.tabs.TabLayout;

import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.delivery.deliveryapp.ui.adapters.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity implements MainCommunicator {

    private ViewPager viewPager;
    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        viewModel.getStopsData().observe(this, stops->{
            sectionsPagerAdapter.setStopsCount(stops.size(), tabs);
        });
    }

    @Override
    public void scrollToStops() {
        viewPager.setCurrentItem(0);
    }
}