package com.delivery.deliveryapp.ui.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.deliveryapp.AppDelegate;
import com.delivery.deliveryapp.R;
import com.delivery.deliveryapp.models.State;
import com.delivery.deliveryapp.models.Stop;
import com.delivery.deliveryapp.ui.views.OvalView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StopsAdapter extends RecyclerView.Adapter<StopsAdapter.StopViewHolder> {

    private ArrayList<Stop> items;
    private AdapterClickListener listener;

    public StopsAdapter() {
        items = new ArrayList<>();
    }

    @NonNull
    @Override
    public StopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StopViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.stop_card_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StopViewHolder holder, int position) {
        holder.idNum.setText(String.valueOf(items.get(position).getId()));
        holder.address.setText(items.get(position).getAddress());
        holder.city.setText(items.get(position).getCity());
        holder.number.setNumber(position + 1);
        holder.number.setState(items.get(position).getState());
        if(items.get(position).getState() == State.IN_PROGRESS){
            holder.background.setVisibility(View.GONE);
            holder.itemView.setBackgroundColor(holder.itemView.getResources().getColor(R.color.white, null));
            holder.actionsLayout.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.setBackgroundColor(holder.itemView.getResources().getColor(R.color.stops_back_color, null));
            holder.actionsLayout.setVisibility(View.GONE);
            if (items.get(position).getState() == State.FINISHED) {
                holder.background.setVisibility(View.GONE);
            } else {
                holder.background.setVisibility(View.VISIBLE);
            }
        }
        if(items.get(position).getState() == State.FINISHED){
            holder.timeZone.setVisibility(View.GONE);
            holder.deliveryTime.setVisibility(View.GONE);
            holder.warning.setVisibility(View.GONE);
            holder.idNum.setTextColor(holder.itemView.getResources().getColor(R.color.gray_3, null));
            holder.address.setTextColor(holder.itemView.getResources().getColor(R.color.gray_3, null));
            holder.city.setTextColor(holder.itemView.getResources().getColor(R.color.gray_3, null));
        } else {
            holder.timeZone.setVisibility(View.VISIBLE);
            holder.deliveryTime.setVisibility(View.VISIBLE);
            holder.idNum.setTextColor(holder.itemView.getResources().getColor(R.color.gray_2, null));
            holder.address.setTextColor(holder.itemView.getResources().getColor(R.color.text_color, null));
            holder.city.setTextColor(holder.itemView.getResources().getColor(R.color.text_color, null));
            holder.timeZone.setText(items.get(position).getTimeZone());
            holder.deliveryTime.setText(items.get(position).getDeliveryTime());

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            try {
                Date date2 = sdf.parse(items.get(position).getTimeZone().substring(6, 11));
                Date date3 = sdf.parse(items.get(position).getDeliveryTime());

                if(date3.after(date2)){
                    holder.deliveryTime.setTextColor(holder.itemView.getResources().getColor(R.color.red, null));
                    holder.warning.setVisibility(View.VISIBLE);
                }else {
                    holder.deliveryTime.setTextColor(holder.itemView.getResources().getColor(R.color.green, null));
                    holder.warning.setVisibility(View.GONE);
                }

            } catch (ParseException e){
                Log.e("StopsAdapter", e.getMessage());
            }
        }

        holder.itemView.setOnClickListener(view -> {
            if(items.get(position).getState() != State.FINISHED){
                if(listener != null){
                    listener.onItemClicked(items.get(position));
                }
            }
        });

        holder.finish.setOnClickListener(view -> {
            if(listener != null){
                listener.onFinishClicked(items.get(position));
            }
        });

        holder.navigate.setOnClickListener(view -> {
            if(listener != null){
                listener.onNavigateClicked(items.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setStopsList(List<Stop> list){
        items.clear();
        items.addAll(list);
        notifyDataSetChanged();
    }

    public interface AdapterClickListener{
        void onItemClicked(Stop stop);
        void onFinishClicked(Stop stop);
        void onNavigateClicked(Stop stop);
    }

    public void setOnAdapterClickListener(AdapterClickListener listener){
        this.listener = listener;
    }

    static class StopViewHolder extends RecyclerView.ViewHolder{
        TextView idNum;
        TextView address;
        TextView city;
        OvalView number;
        View background;
        ConstraintLayout actionsLayout;
        LinearLayout finish;
        LinearLayout navigate;
        TextView timeZone;
        TextView deliveryTime;
        ImageView warning;

        StopViewHolder(@NonNull View itemView) {
            super(itemView);
            idNum = itemView.findViewById(R.id.id_num);
            address = itemView.findViewById(R.id.address);
            city = itemView.findViewById(R.id.city);
            number = itemView.findViewById(R.id.number);
            background = itemView.findViewById(R.id.stop_background);
            actionsLayout = itemView.findViewById(R.id.actions_layout);
            finish = itemView.findViewById(R.id.finish_button);
            navigate = itemView.findViewById(R.id.navigate_button);
            timeZone = itemView.findViewById(R.id.time_zone);
            deliveryTime = itemView.findViewById(R.id.delivery_time);
            warning = itemView.findViewById(R.id.warning_icon);

            setTouchListeners();
        }

        private void setTouchListeners(){
            View.OnTouchListener touchListener = new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                        view.setAlpha(0.5f);
                    } else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                        view.setAlpha(1.0f);
                    }
                    return false;
                }
            };
            finish.setOnTouchListener(touchListener);
            navigate.setOnTouchListener(touchListener);
        }
    }
}
