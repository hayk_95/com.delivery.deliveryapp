package com.delivery.deliveryapp.ui.adapters;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.delivery.deliveryapp.R;
import com.delivery.deliveryapp.ui.fragments.MapFragment;
import com.delivery.deliveryapp.ui.fragments.StopsFragment;
import com.google.android.material.tabs.TabLayout;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private String[] tabsTitles;
    private StopsFragment stopsFragment;
    private MapFragment mapFragment;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        tabsTitles = new String[]{context.getString(R.string.tab_text_1), context.getString(R.string.tab_text_2)};
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (stopsFragment == null) {
                    stopsFragment = new StopsFragment();
                }
                return stopsFragment;
            case 1:
                if(mapFragment == null){
                    mapFragment = new MapFragment();
                }
                return mapFragment;
        }
        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabsTitles[position];
    }

    @Override
    public int getCount() {
        return tabsTitles.length;
    }

    public void setStopsCount(int stopsCount, TabLayout tabs) {
        tabsTitles[0] = tabsTitles[0].substring(0, 5) + '(' + stopsCount + ')';
        tabs.getTabAt(0).setText(tabsTitles[0]);
    }
}