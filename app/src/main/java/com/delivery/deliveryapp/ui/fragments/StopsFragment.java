package com.delivery.deliveryapp.ui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delivery.deliveryapp.viewmodels.MainViewModel;
import com.delivery.deliveryapp.R;
import com.delivery.deliveryapp.models.State;
import com.delivery.deliveryapp.models.Stop;
import com.delivery.deliveryapp.ui.adapters.StopsAdapter;

public class StopsFragment extends Fragment {

    private RecyclerView stopsList;
    private StopsAdapter adapter;
    private MainViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stops, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        viewModel.getStopsData().observe(getViewLifecycleOwner(), stops->{
            adapter.setStopsList(stops);
            int inProgressItem = -1;
            for (Stop stop: stops){
                if(stop.getState() == State.IN_PROGRESS){
                    inProgressItem = stops.indexOf(stop);
                    break;
                }
            }
            if(inProgressItem >= 0 && (((LinearLayoutManager) stopsList.getLayoutManager()).findFirstVisibleItemPosition() <= inProgressItem ||
                    ((LinearLayoutManager) stopsList.getLayoutManager()).findLastVisibleItemPosition() >= inProgressItem)){
                stopsList.scrollToPosition(inProgressItem);
            }
        });
    }

    private void initUI(View view){
        stopsList = view.findViewById(R.id.stops_list);
        stopsList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new StopsAdapter();
        adapter.setOnAdapterClickListener(new StopsAdapter.AdapterClickListener() {
            @Override
            public void onItemClicked(Stop stop) {
                viewModel.updateStopsStates(stop);
            }

            @Override
            public void onFinishClicked(Stop stop) {
                viewModel.finishStop(stop);
            }

            @Override
            public void onNavigateClicked(Stop stop) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + stop.getLatitude() + "," + stop.getLongitude()));
                startActivity(intent);
            }
        });
        stopsList.setAdapter(adapter);
    }
}
