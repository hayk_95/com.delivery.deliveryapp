package com.delivery.deliveryapp.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.delivery.deliveryapp.R;
import com.delivery.deliveryapp.models.State;

public class OvalView extends ConstraintLayout {

    private Paint paint;
    private TextView numText;
    private ImageView iconImg;

    public OvalView(Context context) {
        this(context, null);
    }

    public OvalView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OvalView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        setWillNotDraw(false);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.oval_view_layout, this, true);
        numText = view.findViewById(R.id.num_text);
        iconImg = view.findViewById(R.id.icon_image);
        paint = new Paint();
        paint.setColor(getContext().getColor(R.color.stop_back));
        setState(State.DEFAULT);
    }

    public void setNumber(int number){
        numText.setText(String.valueOf(number));
    }

    public void setState(State state){
        switch (state){
            case DEFAULT:
                numText.setVisibility(VISIBLE);
                iconImg.setVisibility(GONE);
                paint.setColor(getContext().getColor(R.color.gray));
                numText.setTextColor(getContext().getColor(R.color.gray_2));
                break;
            case IN_PROGRESS:
                numText.setVisibility(VISIBLE);
                iconImg.setVisibility(GONE);
                paint.setColor(getContext().getColor(R.color.pink));
                numText.setTextColor(getContext().getColor(R.color.white));
                break;
            case FINISHED:
                numText.setVisibility(GONE);
                iconImg.setVisibility(VISIBLE);
                paint.setColor(getContext().getColor(R.color.blue));
                break;
        }
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawOval(0, 0, getWidth(), getHeight(), paint);
    }
}
