package com.delivery.deliveryapp.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.delivery.deliveryapp.AppDelegate;
import com.delivery.deliveryapp.models.State;
import com.delivery.deliveryapp.models.Stop;

import java.util.List;

public class MainViewModel extends AndroidViewModel {

    private LiveData<List<Stop>> stopsData;

    public MainViewModel(@NonNull Application application) {
        super(application);
        stopsData = AppDelegate.getInstance().getDatabase().stopDao().getStopsLiveData();
    }

    public LiveData<List<Stop>> getStopsData(){
        return stopsData;
    }

    public void updateStopsStates(Stop stop){
        AppDelegate.getInstance().getDatabase().stopDao().updateInProgress();
        if(stop.getState() == State.DEFAULT) {
            stop.setStateInt(State.IN_PROGRESS.num);
            AppDelegate.getInstance().getDatabase().stopDao().update(stop);
        }
    }

    public void finishStop(Stop stop){
        stop.setStateInt(State.FINISHED.num);
        AppDelegate.getInstance().getDatabase().stopDao().update(stop);
    }
}
