package com.delivery.deliveryapp;

import android.app.Application;

import androidx.room.Room;

import com.delivery.deliveryapp.data.AppDatabase;
import com.delivery.deliveryapp.models.Stop;

public class AppDelegate extends Application {

    private static AppDelegate instance;
    private AppDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database =  Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "database")
                .allowMainThreadQueries()
                .build();

        if(database.stopDao().getStops().isEmpty()){
            database.stopDao().insert(new Stop(87488475, "Hanrapetutyan Hraparak", "Yerevan", "09:30-12:30", "12:00", 40.177626, 44.512458));
            database.stopDao().insert(new Stop(87964523, "Abovyan St 1", "Yerevan", "08:30-10:30", "10:40", 40.179167, 44.513610));
            database.stopDao().insert(new Stop(87488404, "Amiryan St 4", "Yerevan", "16:20-17:40", "16:50", 40.178685, 44.510908));
            database.stopDao().insert(new Stop(87488415, "Nalbandyan St 17", "Yerevan", "18:00-21:00", "20:00", 40.178728, 44.515180));
            database.stopDao().insert(new Stop(87488425, "Mashtoc Ave 47", "Yerevan", "09:30-12:30", "12:00", 40.186110, 44.513544));
            database.stopDao().insert(new Stop(87964533, "Sayat-Nova Ave 5", "Yerevan", "08:30-10:30", "09:00", 40.185810, 44.516742));
            database.stopDao().insert(new Stop(87488445, "Marshal Baghramyan Ave 31", "Yerevan", "16:20-17:40", "16:50", 40.192122, 44.503046));
            database.stopDao().insert(new Stop(87488455, "Tumanyan St 21", "Yerevan", "17:00-21:00", "23:00", 40.182954, 44.516417));
            database.stopDao().insert(new Stop(87488465, "Isahakyan St 35", "Yerevan", "09:30-12:30", "12:00", 40.186484, 44.522439));
            database.stopDao().insert(new Stop(87964583, "Proshyan St 28", "Yerevan", "08:30-10:30", "10:20", 40.188580, 44.503782));
            database.stopDao().insert(new Stop(87488495, "Nardos St 60", "Yerevan", "16:20-17:40", "16:50", 40.166638, 44.519405));
            database.stopDao().insert(new Stop(87488175, "Alek Manukyan St 1", "Yerevan", "18:00-21:00", "21:33", 40.181949, 44.526257));
            database.stopDao().insert(new Stop(87488275, "Italy St 5", "Yerevan", "09:30-12:30", "12:00", 40.174649, 44.507252));
            database.stopDao().insert(new Stop(87964323, "Hanrapetutyan St 82", "Yerevan", "08:30-10:30", "10:20", 40.179583, 44.522786));
            database.stopDao().insert(new Stop(87488575, "Mashtoc Ave 39", "Yerevan", "16:20-17:40", "16:50", 40.188745, 44.516047));
            database.stopDao().insert(new Stop(87488675, "Azatutyan Ave 27", "Yerevan", "18:00-21:00", "22:00", 40.210967, 44.529284));
            database.stopDao().insert(new Stop(87488775, "Komitas Ave 60", "Yerevan", "09:30-12:30", "12:00", 40.206175, 44.521383));
            database.stopDao().insert(new Stop(87964823, "Abovyan St 64", "Yerevan", "08:30-10:30", "10:00", 40.191551, 44.528935));
            database.stopDao().insert(new Stop(87488975, "Amiryan St 27", "Yerevan", "16:20-17:40", "16:50", 40.182286, 44.505649));
            database.stopDao().insert(new Stop(87481475, "Kievyan St 1", "Yerevan", "18:00-21:00", "20:00", 40.193083, 44.484102));
        }
    }

    public static AppDelegate getInstance() {
        return instance;
    }

    public AppDatabase getDatabase() {
        return database;
    }
}
