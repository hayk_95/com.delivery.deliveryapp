package com.delivery.deliveryapp.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.delivery.deliveryapp.models.Stop;

import java.util.List;

@Dao
public interface StopDao {

    @Query("SELECT * FROM stop")
    LiveData<List<Stop>> getStopsLiveData();

    @Query("SELECT * FROM stop")
    List<Stop> getStops();

    @Insert
    void insert(Stop stop);

    @Update
    void update(Stop stop);

    @Query("Update stop SET stateInt = 0 WHERE stateInt = 1")
    void updateInProgress();
}
