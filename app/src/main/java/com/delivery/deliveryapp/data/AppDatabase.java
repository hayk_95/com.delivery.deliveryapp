package com.delivery.deliveryapp.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.delivery.deliveryapp.models.Stop;

@Database(entities = {Stop.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract StopDao stopDao();
}
