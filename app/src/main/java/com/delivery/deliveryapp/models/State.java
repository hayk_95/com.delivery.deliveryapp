package com.delivery.deliveryapp.models;

public enum State{
    DEFAULT(0),
    IN_PROGRESS(1),
    FINISHED(2);

    public int num;

    State(int num){
        this.num = num;
    }

    public static State getByNum(int n){
        for (State state: State.values()){
            if(state.num == n){
                return state;
            }
        }
        return null;
    }
}
