package com.delivery.deliveryapp.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Stop {

    public static final int DEFAULT = 0;
    public static final int IN_PROGRESS = 1;
    public static final int FINISHED = 2;

    @PrimaryKey
    private int id;
    private String city;
    private String address;
    private String timeZone;
    private String deliveryTime;
    private double latitude;
    private double longitude;
    private int stateInt;

    public Stop(int id, String address, String city, String timeZone, String deliveryTime, double latitude, double longitude) {
        this.id = id;
        this.city = city;
        this.address = address;
        this.timeZone = timeZone;
        this.deliveryTime = deliveryTime;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public int getStateInt() {
        return stateInt;
    }

    public State getState() {
        return State.getByNum(stateInt);
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public void setStateInt(int stateInt) {
        this.stateInt = stateInt;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
